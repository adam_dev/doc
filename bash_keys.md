Basic moves
===========

* Ctrl + b - move back one character
* Ctrl + f - move forward one character
* Ctrl + d - delete current character
* Backspace - delete previous character
* Ctrl + - - undo

Moving faster
=============

* Ctrl + a - move to the start of line
* Ctrl + e - move to the end of line
* Meta + f - move forward a word
* Meta + b - move backward a word
* Ctrl + l - cear the screen

Cut and paste
=============

* Ctrl + K - cut from cursor to the and of line
* Meta + d - cut from cursor to the and of word
* Meta + Backspace - cut from cursor to the start of word
* Ctrl + w - cut from cursor to the previous whitespace
* Ctrl + y - paste the last cut text
* Meta + y - loop through and paste previously cut text
* Meta + . - loop through and paste the last argument of previous commands

Search the command history
==========================

* Ctrl + r - search as you type
* Ctrl + R - search the last remembered search term
* Ctrl + j - end the serach at current history entry
* Ctrl + g - cancel the search and restore original line