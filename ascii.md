ASCII
=====

Other names
-----------
 - [RFC-20](http://www.rfc-editor.org/rfc/rfc20.txt)
 - ISO/IEC 646-IRV
 - [ECMA-6](http://www.ecma-international.org/publications/standards/Ecma-006.htm)


Table
-----

|HEX CODE|  0x |  1x |  2x |  3x |  4x |  5x |  6x |  7x |
|----:|----:|----:|----:|----:|----:|----:|----:|----:|
|**x0**| NUL | DLE | SP  |  0  |  @  |  P  |  `  |  p  |
|**x1**| SOH | DC1 |  !  |  1  |  A  |  Q  |  a  |  q  |
|**x2**| STX | DC2 |  "  |  2  |  B  |  R  |  b  |  r  |
|**x3**| ETX | DC3 |  #  |  3  |  C  |  S  |  c  |  s  |
|**x4**| EOT | DC4 |  $  |  4  |  D  |  T  |  d  |  t  |
|**x5**| ENQ | NAK |  %  |  5  |  E  |  U  |  e  |  u  |
|**x6**| ACK | SYN |  &  |  6  |  F  |  V  |  f  |  v  |
|**x7**| BEL | ETB |  '  |  7  |  G  |  W  |  g  |  w  |
|**x8**| BS  | CAN |  (  |  8  |  H  |  X  |  h  |  x  |
|**x9**| HT  | EM  |  )  |  9  |  I  |  Y  |  i  |  y  |
|**xA**| LF  | SUB |  *  |  :  |  J  |  Z  |  j  |  z  |
|**xB**| VT  | ESC |  +  | ;   |  K  |  [  |  k  |  {  |
|**xC**| FF  | FS  |  ,  |  <  |  L  |  \  |  l  |&#124;|
|**xD**| CR  | GS  |  -  |  =  |  M  |  ]  |  m  |  }  |
|**xE**| SO  | RS  |  .  |  >  |  N  |  ^  |  n  |  ~  |
|**xF**| SI  | US  |  /  |  ?  |  O  |  _  |  o  | DEL |

Legend of Control Chararcters:

|symbol|meaning|symbol|meaning|
|:---:|:---|:---:|:---|
|NUL|Null     |DLE|Data Link Escape (CC)|
|SOH|Start of Heading (CC)|DC1|Device Control 1|
|STX|Start of Text (CC)|DC2|Device Control 2|
|ETX|End of Text (CC)|DC3|Device Control 3|
|EOT|End of Transmission (CC)|DC4|Device Control 4|
|ENQ|Enquiry (CC)|NAK|Negative Acknowledge (CC)|
|ACK|Acknowledge (CC)|SYN|Synchronous Idle (CC)|
|BEL|Bell (audible or attention signal)|ETB|End of Transmission Block (CC)|
|BS|Backspace (FE)|CAN|Cancel|
|HT|Horizontal Tabulation (punched card skip) (FE)|EM|End of Medium|
|LF|Line Feed (FE)|SUB|Substitute|
|VT|Vertical Tabulation (FE)|FS|File Separator (IS)|
|FF|Form Feed (FE)|GS|Group Separator (IS)|
|CR|Carriage Return (FE)|RS|Record Separator (IS)|
|SO|Shift Out|DEL|Delete (CC)|
|SI|Shift In|

Where:

|symbol|meaning|
|:---:|:---|
|CC|Communication Control|
|FE|Format Effector|
|IS|Information Separator|

Layout
------

```
╔═══════════╦═════╦═════════════════════════════╗
║           ║ SP  ║                             ║
║           ╠═════╝                             ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║    C0     ║                G0                 ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                                   ║
║           ║                             ╔═════╣
║           ║                             ║ DEL ║
╚═══════════╩═════════════════════════════╩═════╝
```

Mapping
-------

Unique graphic character allocations:

|Graphic Symbol|Name|Coded representation|
|:---:|:---|---:|
|!|EXCLAMATION MARK|2/1|
|"|QUOTATION MARK|2/2|
|%|PERCENT SIGN|2/5|
|&|AMPERSAND|2/6|
|'|APOSTROPHE|2/7|
|(|LEFT PARENTHESIS|2/8|
|)|RIGHT PARENTHESIS|2/9|
|*|ASTERISK|2/10|
|+|PLUS SIGN|2/11|
|,|COMMA|2/12|
|-|HYPHEN-MINUS|2/13|
|.|FULL STOP|2/14|
|/|SOLIDUS|2/15|
|0|DIGIT ZERO|3/0|
|1|DIGIT ONE|3/1|
|2|DIGIT TWO|3/2|
|3|DIGIT THREE|3/3|
|4|DIGIT FOUR|3/4|
|5|DIGIT FIVE|3/5|
|6|DIGIT SIX|3/6|
|7|DIGIT SEVEN|3/7|
|8|DIGIT EIGHT|3/8|
|9|DIGIT NINE|3/9|
|:|COLON|3/10|
|;|SEMICOLON|3/11|
|<|LESS-THAN SIGN|3/12|
|=|EQUALS SIGN|3/13|
|>|GREATER-THAN SIGN|3/14|
|?|QUESTION MARK|3/15|
|A|LATIN CAPITAL LETTER A|4/1|
|B|LATIN CAPITAL LETTER B|4/2|
|C|LATIN CAPITAL LETTER C|4/3|
|D|LATIN CAPITAL LETTER D|4/4|
|E|LATIN CAPITAL LETTER E|4/5|
|F|LATIN CAPITAL LETTER F|4/6|
|G|LATIN CAPITAL LETTER G|4/7|
|H|LATIN CAPITAL LETTER H|4/8|
|I|LATIN CAPITAL LETTER I|4/9|
|J|LATIN CAPITAL LETTER J|4/10|
|K|LATIN CAPITAL LETTER K|4/11|
|L|LATIN CAPITAL LETTER L|4/12|
|M|LATIN CAPITAL LETTER M|4/13|
|N|LATIN CAPITAL LETTER N|4/14|
|O|LATIN CAPITAL LETTER O|4/15|
|P|LATIN CAPITAL LETTER P|5/0|
|Q|LATIN CAPITAL LETTER Q|5/1|
|R|LATIN CAPITAL LETTER R|5/2|
|S|LATIN CAPITAL LETTER S|5/3|
|T|LATIN CAPITAL LETTER T|5/4|
|U|LATIN CAPITAL LETTER U|5/5|
|V|LATIN CAPITAL LETTER V|5/6|
|W|LATIN CAPITAL LETTER W|5/7|
|X|LATIN CAPITAL LETTER X|5/8|
|Y|LATIN CAPITAL LETTER Y|5/9|
|Z|LATIN CAPITAL LETTER Z|5/10|
|_|LOW LINE|5/15|
|a|LATIN SMALL LETTER A|6/1|
|b|LATIN SMALL LETTER B|6/2|
|c|LATIN SMALL LETTER C|6/3|
|d|LATIN SMALL LETTER D|6/4|
|e|LATIN SMALL LETTER E|6/5|
|f|LATIN SMALL LETTER F|6/6|
|g|LATIN SMALL LETTER G|6/7|
|h|LATIN SMALL LETTER H|6/8|
|i|LATIN SMALL LETTER I|6/9|
|j|LATIN SMALL LETTER J|6/10|
|k|LATIN SMALL LETTER K|6/11|
|l|LATIN SMALL LETTER L|6/12|
|m|LATIN SMALL LETTER M|6/13|
|n|LATIN SMALL LETTER N|6/14|
|o|LATIN SMALL LETTER O|6/15|
|p|LATIN SMALL LETTER P|7/0|
|q|LATIN SMALL LETTER Q|7/1|
|r|LATIN SMALL LETTER R|7/2|
|s|LATIN SMALL LETTER S|7/3|
|t|LATIN SMALL LETTER T|7/4|
|u|LATIN SMALL LETTER U|7/5|
|v|LATIN SMALL LETTER V|7/6|
|w|LATIN SMALL LETTER W|7/7|
|x|LATIN SMALL LETTER X|7/8|
|y|LATIN SMALL LETTER Y|7/9|
|z|LATIN SMALL LETTER Z|7/10|

Alternative graphic character allocations:

|Graphic Symbol|Name|Coded representation|
|:---:|:---|---:|
|#|NUMBER SIGN|2/3|
|£|POUND SIGN|2/3|
|$|DOLLAR SIGN|2/4|
||CURRENCY SIGN|2/4|

IRV graphic character allocations:

Graphic Symbol|Name|Coded representation
:---:|:---|---:
#|NUMBER SIGN|2/3
$|DOLLAR SIGN|2/4
@|COMMERCIAL AT|4/0
[|LEFT SQUARE BRACKET|5/11
\|REVERSE SOLIDUS|5/12
]|RIGHT SQUARE BRACKET|5/13
^|CIRCUMFLEX ACCENT|5/14
`|GRAVE ACCENT|6/0
{|LEFT CURLY BRACKET|7/11
&#124;|VERTICAL LINE|7/12
}|RIGHT CURLY BRACKET|7/13
∼|TILDE|7/14
